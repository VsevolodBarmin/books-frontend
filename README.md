# books-frontend

## You need to make .env file so that the frontend app can get the proper backend API url.
## You can simply duplicate the .env.example file, rename it into .env and edit the environment variables in it.

## Project setup
```
npm install
```

### Before you are going to run, please make sure that the value of VUE_APP_API_BASE in .env matches the backend API url.
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
