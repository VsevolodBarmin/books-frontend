import Axios from "axios";

export default {
  getBooks: (data) => {
    const { page, limit, search } = data;

    return Axios.get(`books?page=${page}&limit=${limit}&search=${search}`);
  },

  uploadBooksXmlFile: (xmlFile) => {
    let formData = new FormData();

    formData.append("xml", xmlFile);

    return Axios.post(`upload-books-xml`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
};
