import Vue from "vue";
import Axios from "axios";
import {
  BIconSearch,
  BootstrapVue,
  BSpinner,
  IconsPlugin,
} from "bootstrap-vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import App from "./App.vue";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.component("b-icon-search", BIconSearch);
Vue.component("b-spinner", BSpinner);

Axios.defaults.baseURL = process.env.VUE_APP_API_BASE;

new Vue({
  render: (hh) => hh(App),
}).$mount("#app");
